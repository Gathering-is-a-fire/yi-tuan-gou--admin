# 疫团 Go - 管理后台

>本网站属于公益项目，由上海电子信息职业技术学院【聚是一团火】团队开发和维护，数据来源于自发组织的志愿者们收集和整理。

## 介绍

本项目基于 vue-vben-admin（是一个免费开源的中后台模版）开发，正在开发中。

- 疫团 Go-移动端

  - 码云地址：[https://gitee.com/Gathering-is-a-fire/yi-tuan-gou--unapp](https://gitee.com/Gathering-is-a-fire/yi-tuan-gou--unapp)

- 疫团 Go-服务端

  - 码云地址：[https://gitee.com/Gathering-is-a-fire/yi-tuan-gou--server](https://gitee.com/Gathering-is-a-fire/yi-tuan-gou--server)

- 疫团 Go-管理后台
  - 码云地址：[https://gitee.com/Gathering-is-a-fire/yi-tuan-gou--admin](https://gitee.com/Gathering-is-a-fire/yi-tuan-gou--admin)

### 贡献名单

聚是一团火，散是满天星。

![donate](https://tg.xuan-niao.com/name-list.png?v=3)

## 特性

- **最新技术栈**：使用 Vue3/vite2 等前端前沿技术开发
- **TypeScript**: 应用程序级 JavaScript 的语言
- **主题**：可配置的主题
- **国际化**：内置完善的国际化方案
- **Mock 数据** 内置 Mock 数据方案
- **权限** 内置完善的动态路由权限生成方案
- **组件** 二次封装了多个常用的组件

## 安装使用

- 获取项目代码

```bash
git clone https://gitee.com/Gathering-is-a-fire/yi-tuan-gou--admin
```

- 安装依赖

```bash
cd yi-tuan-gou--admin

pnpm install

```

- 运行

```bash
pnpm serve
```

- 打包

```bash
pnpm build
```


### 注意

- **直接部署或二次开发后部署请声明来源【上海电子信息职业技术学院-聚是一团火】团队**
