import { defHttp } from '/@/utils/http/axios';

enum Api {
  product = '/goods/product',
}

/**
 * @description: 商品信息-查询
 */

export const getProductApi = (params) =>
  defHttp.get({
    url: Api.product,
    params,
  });

/**
 * @description: 商品信息-新增
 */

export const addProductApi = (params) =>
  defHttp.post({
    url: Api.product,
    params,
  });

/**
 * @description: 商品信息-删除
 */

export const delProductApi = (params) =>
  defHttp.delete({
    url: Api.product,
    params,
  });

/**
 * @description: 商品信息-修改
 */

export const modProductApi = (params) =>
  defHttp.put({
    url: Api.product,
    params,
  });
