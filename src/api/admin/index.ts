import { defHttp } from '/@/utils/http/axios';

enum Api {
  captcha = '/admin/captcha',
}

/**
 * @description: 获取 - 验证码
 */

export const getCaptcha = (params) =>
  defHttp.get({
    url: Api.captcha,
    params,
  });
