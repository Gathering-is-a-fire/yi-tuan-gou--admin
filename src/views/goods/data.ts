import { FormSchema } from '/@/components/Table';

export const formSchema: FormSchema[] = [
  {
    field: 'id',
    component: 'InputNumber',
    label: 'ID',
    show: false,
  },
  {
    field: 'productName',
    label: '名称',
    component: 'Input',
    // 感叹号提示
    // helpMessage: ['本字段演示异步验证', '不能输入带有admin的姓名'],
    rules: [
      {
        required: true,
        message: '请输入物资名称',
      },
      // {
      //   validator(_, value) {
      //     return new Promise((resolve, reject) => {
      //       isAccountExist(value)
      //         .then(() => resolve())
      //         .catch((err) => {
      //           reject(err.message || '验证失败');
      //         });
      //     });
      //   },
      // },
    ],
  },
  {
    field: 'price',
    label: '价格',
    component: 'Input',
    rules: [
      {
        message: '请输入价格',
      },
    ],
  },
  {
    field: 'productCategoryId',
    label: '商品类型',
    component: 'RadioGroup',
    required: true,
    defaultValue: 0,
    componentProps: {
      options: [
        { value: 1, label: '主食' },
        { value: 2, label: '零食' },
        { value: 3, label: '母婴用品' },
        { value: 4, label: '女性用品' },
        { value: 5, label: '酒水饮料' },
        { value: 6, label: '日用品' },
        { value: 7, label: '团购平台' },
        { value: 8, label: '超市' },
        { value: 9, label: '药品保健' },
        { value: 11, label: '团餐配送' },
        { value: 12, label: '加油站' },
        { value: 13, label: '宠物' },
      ],
    },
  },
  {
    field: 'deliverArea',
    label: '配送地区',
    component: 'CheckboxGroup',
    required: true,
    defaultValue: 0,
    componentProps: {
      options: [
        { value: 0, label: '全市' },
        { value: 1, label: '黄浦' },
        { value: 2, label: '徐汇' },
        { value: 3, label: '长宁' },
        { value: 4, label: '静安' },
        { value: 5, label: '普陀' },
        { value: 6, label: '虹口' },
        { value: 7, label: '杨浦' },
        { value: 8, label: '闵行' },
        { value: 9, label: '宝山' },
        { value: 10, label: '嘉定' },
        { value: 11, label: '浦东' },
        { value: 12, label: '金山' },
        { value: 13, label: '松江' },
        { value: 14, label: '青浦' },
        { value: 15, label: '奉贤' },
        { value: 16, label: '崇明' },
      ],
    },
  },
  {
    field: 'purchaseNotes',
    label: '起送要求',
    component: 'Input',
    rules: [
      {
        message: '请输入起送要求',
      },
    ],
  },
  {
    field: 'contactPeo',
    label: '联系人',
    component: 'Input',
    rules: [
      {
        message: '请输入联系人',
      },
    ],
  },
  {
    field: 'contactPhone',
    label: '联系方式',
    component: 'Input',
    rules: [
      {
        // required: true,
        message: '请输入联系方式',
      },
    ],
  },
  {
    field: 'supportCompany',
    label: '提供企业',
    component: 'Input',
    rules: [
      {
        message: '请输入提供企业',
      },
    ],
  },
  {
    field: 'infoResourse',
    label: '消息来源',
    component: 'Input',
    rules: [
      {
        required: true,
        message: '请输入消息来源',
      },
    ],
  },
  {
    field: 'addressType',
    label: '来源类型',
    component: 'RadioGroup',
    required: true,
    defaultValue: 0,
    componentProps: {
      options: [
        { value: '1', label: '图片' },
        { value: '2', label: '原文链接' },
      ],
    },
  },
  {
    field: 'productAddress',
    label: '来源链接',
    component: 'Input',
    rules: [
      {
        required: true,
        message: '请输入来源链接',
      },
    ],
  },
  {
    field: 'releaseTime',
    label: '发布时间',
    component: 'Input',
    rules: [
      {
        required: true,
        message: '请输入发布时间',
      },
    ],
  },
];
