import { FormProps } from '/@/components/Table';
import { BasicColumn } from '/@/components/Table/src/types/table';

export function getBasicColumns(): BasicColumn[] {
  return [
    {
      title: 'ID',
      dataIndex: 'id',
      fixed: 'left',
      width: 100,
    },
    {
      title: '名称',
      dataIndex: 'productName',
      width: 120,
    },
    {
      title: '价格',
      dataIndex: 'price',
      width: 120,
    },
    {
      title: '商品类型',
      dataIndex: 'productCategory',
      width: 120,
    },
    {
      title: '配送地区',
      dataIndex: 'deliverArea',
      width: 120,
    },
    {
      title: '起送要求',
      dataIndex: 'purchaseNotes',
      width: 180,
    },
    {
      title: '联系人',
      dataIndex: 'contactPeo',
      width: 150,
    },
    {
      title: '联系方式',
      dataIndex: 'contactPhone',
      width: 200,
    },
    {
      title: '提供企业',
      dataIndex: 'supportCompany',
      width: 200,
    },
    {
      title: '消息来源',
      dataIndex: 'infoResourse',
      width: 200,
    },
    {
      title: '来源类型',
      dataIndex: 'addressType',
      width: 80,
    },
    {
      title: '来源链接',
      dataIndex: 'productAddress',
      width: 120,
    },
    {
      title: '发布时间',
      dataIndex: 'releaseTime',
      width: 120,
    },
  ];
}

// 搜索栏 表单配置
export function getFormConfig(): Partial<FormProps> {
  return {
    labelWidth: 100,
    schemas: [
      {
        field: 'keywords',
        label: '模糊查询',
        component: 'Input',
        colProps: {
          xl: 6,
          xxl: 4,
        },
      },
      {
        field: 'categoryId',
        label: '商品类型',
        component: 'Select',
        colProps: {
          xl: 6,
          xxl: 4,
        },
        componentProps: {
          options: [
            { id: 1, label: '主食', value: '1', key: '1' },
            { id: 2, label: '零食', value: '2', key: '2' },
            { id: 3, label: '母婴用品', value: '3', key: '3' },
            { id: 4, label: '女性用品', value: '4', key: '4' },
            { id: 5, label: '酒水饮料', value: '5', key: '5' },
            { id: 6, label: '日用品', value: '6', key: '6' },
            { id: 7, label: '团购平台', value: '7', key: '7' },
            { id: 8, label: '超市', value: '8', key: '8' },
            { id: 9, label: '药品保健', value: '9', key: '9' },
            { id: 11, label: '团餐配送', value: '11', key: '11' },
            { id: 12, label: '加油站', value: '12', key: '12' },
            { id: 13, label: '宠物', value: '13', key: '13' },
          ],
        },
      },
      {
        field: 'areaId',
        label: '配送地区',
        component: 'Select',
        colProps: {
          xl: 6,
          xxl: 4,
        },
        componentProps: {
          options: [
            { id: 1, label: '黄浦', value: '1', key: '1' },
            { id: 2, label: '徐汇', value: '2', key: '2' },
            { id: 3, label: '长宁', value: '3', key: '3' },
            { id: 4, label: '静安', value: '4', key: '4' },
            { id: 5, label: '普陀', value: '5', key: '5' },
            { id: 6, label: '虹口', value: '6', key: '6' },
            { id: 7, label: '杨浦', value: '7', key: '7' },
            { id: 8, label: '闵行', value: '8', key: '8' },
            { id: 9, label: '宝山', value: '9', key: '9' },
            { id: 10, label: '嘉定', value: '10', key: '10' },
            { id: 11, label: '浦东', value: '11', key: '11' },
            { id: 12, label: '金山', value: '12', key: '12' },
            { id: 13, label: '松江', value: '13', key: '13' },
            { id: 14, label: '青浦', value: '14', key: '14' },
            { id: 15, label: '奉贤', value: '15', key: '15' },
            { id: 16, label: '崇明', value: '16', key: '16' },
          ],
        },
      },
      // ...getAdvanceSchema(5),
      // {
      //   field: `field11`,
      //   label: `Slot示例`,
      //   component: 'Select',
      //   slot: 'custom',
      //   colProps: {
      //     xl: 12,
      //     xxl: 8,
      //   },
      // },
    ],
  };
}
