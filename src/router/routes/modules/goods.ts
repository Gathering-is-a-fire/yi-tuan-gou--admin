import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const dashboard: AppRouteModule = {
  path: '/goods',
  name: 'Goods',
  component: LAYOUT,
  redirect: '/goods/index',
  meta: {
    hideChildrenInMenu: true,
    // icon: 'ant-design:home-outlined',
    // title: t('routes.dashboard.about'),
    title: '物资管理',
    orderNo: 100000,
  },
  children: [
    {
      path: 'index',
      name: 'GoodsPage',
      component: () => import('/@/views/goods/index.vue'),
      meta: {
        title: '物资列表',
        icon: 'simple-icons:about-dot-me',
        hideMenu: true,
      },
    },
  ],
};

export default dashboard;
